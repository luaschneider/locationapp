package com.example.gocode.locationapp.models;

import java.io.Serializable;

public class Friend implements Serializable{

    //ATTRIBUTES
    private String id;
    private String customName;

    //CONSTRUCTOR
    public Friend(String id, String customName) {
        this.id = id;
        this.customName = customName;
    }

    //GETTERS AND SETTERS
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }
}
