package com.example.gocode.locationapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.SharedMethods;

public class DetailsFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    private Switch tracker_switch;
    private SharedPreferences trackerPrefs;
    private SharedPreferences.Editor trackerPrefsEditor;

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance() {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        trackerPrefs = getContext().getSharedPreferences(
                getString(R.string.preference_tracker), Context.MODE_PRIVATE);
        trackerPrefsEditor = trackerPrefs.edit();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedPreferences accountSharedPrefs = getContext().getSharedPreferences(
                getString(R.string.preference_account_details), Context.MODE_PRIVATE);

        View view = inflater.inflate(R.layout.fragment_details, container, false);

        TextView txt_id = (TextView) view.findViewById(R.id.details_id_value);
        Button btn_share_id = (Button) view.findViewById(R.id.details_id_share);
        tracker_switch = (Switch) view.findViewById(R.id.details_tracker_switch);

        // pega o ID do usuário e coloca no txt
        final String user_id = SharedMethods.getFormattedId(accountSharedPrefs.getInt(getString(R.string.saved_id), 0));
        txt_id.setText(user_id);

        // seta o estado do switch
        tracker_switch.setChecked(trackerPrefs.getBoolean(getString(R.string.is_tracker_enabled), false));
        tracker_switch.setOnCheckedChangeListener(this);

        btn_share_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIdIntent = new Intent(Intent.ACTION_SEND);
                shareIdIntent.putExtra(Intent.EXTRA_TEXT, String.format(getString(R.string.main_details_id_share_message), user_id));
                shareIdIntent.setType("text/plain");

                String title = getResources().getString(R.string.main_details_id_share);
                // Create intent to show the chooser dialog
                Intent chooser = Intent.createChooser(shareIdIntent, title);

                // Verify the original intent will resolve to at least one activity
                if (shareIdIntent.resolveActivity(getContext().getPackageManager()) != null) {
                    startActivity(chooser);
                }
            }
        });

        return view;
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        switch(compoundButton.getId()){

            case R.id.details_tracker_switch:

                trackerPrefsEditor.putBoolean(getString(R.string.is_tracker_enabled), b);
                trackerPrefsEditor.commit();

                break;

        }

    }
}
