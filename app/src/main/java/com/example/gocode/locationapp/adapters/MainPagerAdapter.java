package com.example.gocode.locationapp.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.fragments.FriendsFragment;
import com.example.gocode.locationapp.fragments.DetailsFragment;
import com.example.gocode.locationapp.fragments.MapFragment;

import java.lang.ref.WeakReference;
import java.util.Hashtable;

/**
 * Created by Go Code on 30/10/2016.
 */

public class MainPagerAdapter extends FragmentStatePagerAdapter {

    final int PAGE_COUNT = 3;
    protected Hashtable<Integer, WeakReference<Fragment>> fragmentReferences;
    private Context context;
    private final int TAB_TITLES[] = new int[]{
            R.string.main_tablayout_title_details,
            R.string.main_tablayout_title_map,
            R.string.main_tablayout_title_friends
    };

    public MainPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        fragmentReferences = new Hashtable<>();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = DetailsFragment.newInstance();
                break;
            case 1:
                fragment = MapFragment.newInstance();
                break;
            default:
                fragment = FriendsFragment.newInstance();
                break;
        }
        fragmentReferences.put(position, new WeakReference<>(fragment));
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
//        return context.getString(TAB_TITLES[position]);
    }

    public Fragment getFragment(int fragmentId) {
        WeakReference<Fragment> ref = fragmentReferences.get(fragmentId);
        return ref == null ? null : ref.get();
    }

    public void updateFriendsFragment(Context context) {
        FriendsFragment fragment = (FriendsFragment) getFragment(1);
        if (fragment != null) {
            fragment.updateFriendsList();
        }
    }


}
