package com.example.gocode.locationapp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.example.gocode.locationapp.fragments.HintsIdFragment;
import com.example.gocode.locationapp.fragments.HintsNotificationFragment;

import java.lang.ref.WeakReference;
import java.util.Hashtable;

/**
 * Created by Go Code on 01/11/2016.
 */

public class HintsPagerAdapter extends FragmentStatePagerAdapter {
    final int PAGE_COUNT = 2;
    protected Hashtable<Integer, WeakReference<Fragment>> fragmentReferences;

    public HintsPagerAdapter(FragmentManager fm) {
        super(fm);
        fragmentReferences = new Hashtable<>();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = HintsIdFragment.newInstance();
                break;
            default:
                fragment = HintsNotificationFragment.newInstance();
                break;
        }
        fragmentReferences.put(position, new WeakReference<>(fragment));
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    public Fragment getFragment(int fragmentId) {
        WeakReference<Fragment> ref = fragmentReferences.get(fragmentId);
        return ref == null ? null : ref.get();
    }

    public void idFragmentSetter(int id) {
        HintsIdFragment fragment = (HintsIdFragment) getFragment(0);

        if (fragment != null) {
            fragment.setIdTxtValue(id);
        }
    }

}
