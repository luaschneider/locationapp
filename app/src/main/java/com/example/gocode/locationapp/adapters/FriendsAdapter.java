package com.example.gocode.locationapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.models.Friend;

import java.util.ArrayList;

public class FriendsAdapter extends BaseAdapter {

    private ArrayList<Friend> friends_list;
    private Context context;

    public FriendsAdapter(ArrayList<Friend> friends_list, Context context) {
        this.friends_list = friends_list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return friends_list.size();
    }

    @Override
    public Object getItem(int i) {
        return friends_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;

        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.list_friends_item_layout, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        Friend friend = friends_list.get(position);

        holder.name.setText(friend.getCustomName());
        holder.id.setText(friend.getId());

        return view;
    }

    public class ViewHolder {

        final TextView name, id;

        public ViewHolder(View view) {
            name = (TextView) view.findViewById(R.id.friends_item_txt_name);
            id   = (TextView) view.findViewById(R.id.friends_item_txt_id);
        }
    }
}