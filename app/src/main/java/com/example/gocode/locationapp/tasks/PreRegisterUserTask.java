package com.example.gocode.locationapp.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.gocode.locationapp.WebHelper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class PreRegisterUserTask extends AsyncTask<Void, Void, String> {

    private PreRegisterUserCallback callback;
    private String device_id;

    public PreRegisterUserTask(PreRegisterUserCallback callback, String device_id) {
        this.callback = callback;
        this.device_id = device_id;
    }

    @Override
    protected String doInBackground(Void... voids) {

        Log.d("CHEGOU", "PreRegisterUserTask -> doInBackground()");

        HttpURLConnection connection = null;
        try {
            Log.d("CHEGOU", "PreRegisterUserTask -> doInBackground() -> try{}");

            URL url = new URL(WebHelper.HOST_NAME + WebHelper.PRE_REGISTER_USER_FILE);
            String urlParameters =
                    "device_id=" + URLEncoder.encode(device_id, "UTF-8");
            Log.d("VALUE", "PreRegisterUserTask ->\n String device_id = "+ device_id);

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(5000);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            //Send request
            DataOutputStream wr = new DataOutputStream (
                    connection.getOutputStream ());
            wr.writeBytes (urlParameters);
            wr.flush ();
            wr.close ();

            connection.connect();

            int connResponse = connection.getResponseCode();
            Log.d("VALUE", "PreRegisterUserTask -> int connResponse = " + connResponse);

            //Get Response

            Log.d("VALUE", "PreRegisterUserTask ->\n\t int connection.getResponseCode() = "+ connection.getResponseCode());

            if (connResponse == HttpURLConnection.HTTP_OK) {
                Log.d("CHEGOU", "PreRegisterUserTask -> connection.getRespondeCode() == HTTP_OK");
                InputStream inputStream = connection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();
                connection.disconnect();

                String strResult = sb.toString().substring(1);
                Log.d("VALUE", "PreRegisterUserTask -> String strResult = " + strResult);
                return strResult;
            }

        }catch(Exception e){
            e.printStackTrace();
        }finally{
            if(connection != null){
                connection.disconnect();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(s == null){
            s = "{\"error\" : 10}";
        }else if(s.isEmpty()){
            s = "{\"error\" : 10}";
        }

        callback.processPreRegisterUser(s);
    }

    public interface PreRegisterUserCallback{
        void processPreRegisterUser(String string);
    }
}
