package com.example.gocode.locationapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.SharedMethods;
import com.example.gocode.locationapp.tasks.PreRegisterUserTask;
import com.example.gocode.locationapp.tasks.RegisterUserTask;

import org.json.JSONException;
import org.json.JSONObject;

public class WelcomeActivity extends AppCompatActivity implements PreRegisterUserTask.PreRegisterUserCallback, RegisterUserTask.RegisterUserTaskCallback, SharedMethods.ErrorDialogCallback {

    private Button welcome_next_btn;
    private ProgressDialog progressDialog;

    private SharedPreferences sharedPref;
    private String device_id;

    private int id_from_parse;
    private String token_from_parse;

    private final int REQUEST_CODE_HINTS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        // Device ID
        device_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("VALUE", "WelcomeAcitivty ->\n\t String device_id = " + device_id);

        // Shared Preferences
        sharedPref = this.getSharedPreferences(
                getString(R.string.preference_account_details), Context.MODE_PRIVATE);

        welcome_next_btn = (Button) findViewById(R.id.welcome_next_btn);

        welcome_next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreRegisterUserTask prut = new PreRegisterUserTask(WelcomeActivity.this, device_id);
                prut.execute();
                Log.d("CHEGOU", "WelcomeActivity ->\n\t prut.execute();");

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE_HINTS && resultCode == RESULT_OK){

            progressDialog = new ProgressDialog(this);

            progressDialog.setTitle(R.string.welcome_dialog_title);
            progressDialog.setMessage(getString(R.string.welcome_dialog_message));
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();

            RegisterUserTask rut = new RegisterUserTask(this, id_from_parse, device_id, token_from_parse);
            rut.execute();
        }
    }

    @Override
    public void processPreRegisterUser(String string) {

        try {
            // Parse JSON
            JSONObject jsonObject= new JSONObject(string);

            int error = jsonObject.getInt("error");
            Log.d("VALUE", "WelcomeAcitivty ->\n\t int error (from parser) = " + error);

            // verifica se houve erro
            if(error == 0) {

                id_from_parse = jsonObject.getInt("account_id");
                token_from_parse = jsonObject.getString("token");

                Log.d("VALUE", "WelcomeAcitivty ->\n\t int id_from_parse (from parser) = " + id_from_parse);
                Log.d("VALUE", "WelcomeAcitivty ->\n\t String token_from_parse (from parser) = " + token_from_parse);

                // Write preferences
                SharedPreferences.Editor sharedPrefEditor = sharedPref.edit();

                sharedPrefEditor.putString(getString(R.string.saved_token), token_from_parse);
                sharedPrefEditor.putInt(getString(R.string.saved_id), id_from_parse);
                sharedPrefEditor.commit();

                // Start hints activity
                Intent hintsIntent = new Intent(WelcomeActivity.this, HintsActivity.class);
                startActivityForResult(hintsIntent, REQUEST_CODE_HINTS);

            }else{

                switch(error) {
                    case SharedMethods.CONNECTION_ERROR:
                        AlertDialog errorConnDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_10_message);
                        break;

                    default:
                        AlertDialog errorDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_22_message);
                }
            }
        }catch(JSONException e){
            e.printStackTrace();
        }

    }

    @Override
    public void processRegisterUserTask(String s) {

        try {
            // Parse JSON
            JSONObject jsonObject = new JSONObject(s);

            int error = jsonObject.getInt("error");
            Log.d("VALUE", "WelcomeAcitivty ->\n\t processRegisterUserTask() ->\n\t\t String error (from parser) = " + error);

            // verifica se houve erro
            if(error == 0) {

                String new_token = jsonObject.getString("token");
                Log.d("VALUE", "WelcomeAcitivty ->\n\t processRegisterUserTask() ->\n\t\t String new_token (from parser) = " + new_token);

                // Write preferences
                SharedPreferences.Editor sharedPrefEditor = sharedPref.edit();

                sharedPrefEditor.putString(getString(R.string.saved_token), new_token);
                sharedPrefEditor.commit();

                progressDialog.dismiss();

                SharedMethods.startActivityAndFinish(this, this, MainActivity.class);

            }else{

                switch(error) {
                    case SharedMethods.CONNECTION_ERROR:
                        AlertDialog errorConnDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_10_message);
                        break;

                    default:
                        AlertDialog errorDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_23_message);
                }
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClickPositiveButton(int error) {

    }

    @Override
    public void onClickNegativeButton(int error) {

    }
}
