package com.example.gocode.locationapp.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.gocode.locationapp.WebHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Go Code on 05/11/2016.
 */

public class CheckDeviceRegisteredTask extends AsyncTask<String, Void, String> {

    private CheckDeviceRegisteredCallback callback;

    public CheckDeviceRegisteredTask(CheckDeviceRegisteredCallback callback) {
        this.callback = callback;
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpURLConnection connection = null;
        try {
            String device_id = strings[0];
            String device_id_param = "?device_id="+device_id;
            Log.d("VALUE", "CheckDeviceRegisteredTask ->\n\t String device_id = "+ device_id);

            URL url = new URL(WebHelper.HOST_NAME + WebHelper.IS_DEVICE_REGISTERED_FILE + device_id_param);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.setUseCaches(false);

            Log.d("VALUE", "CheckDeviceRegisteredTask ->\n\t int connection.getResponseCode() = "+ connection.getResponseCode());
            Log.d("VALUE", "CheckDeviceRegisteredTask ->\n\t int connection.getUsesCache() = "+ connection.getUseCaches());

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = connection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();

                String strResult = sb.toString().substring(1);
                Log.d("VALUE", "CheckDeviceRegisteredTask ->\n\t String strResult = " + strResult);
//                Boolean result = Boolean.valueOf(strResult);
                return strResult;
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(s == null){
            s = "{\"error\" : 10}";
        }else if(s.isEmpty()){
            s = "{\"error\" : 10}";
        }

        callback.processIsDeviceRegistered(s);
    }

    //Calback
    public interface CheckDeviceRegisteredCallback {
        void processIsDeviceRegistered(String string);
    }
}
