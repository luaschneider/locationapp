package com.example.gocode.locationapp.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;
import android.widget.ListView;
import android.widget.Toast;

import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.SharedMethods;
import com.example.gocode.locationapp.adapters.FriendRequestsAdapter;
import com.example.gocode.locationapp.adapters.FriendsAdapter;
import com.example.gocode.locationapp.models.Friend;
import com.example.gocode.locationapp.tasks.GetRequestsListTask;
import com.example.gocode.locationapp.tasks.RespondFriendRequestTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FriendRequestsActivity extends AppCompatActivity implements GetRequestsListTask.GetRequestsListTaskCallback, View.OnClickListener, RespondFriendRequestTask.RespondFriendRequestTaskCallback, SharedMethods.ErrorDialogCallback {

    private ViewStub viewStub;
    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;

    private FriendRequestsAdapter requestsAdapter;
    private ArrayList<Integer> requests = new ArrayList<>();

    private String device_id;
    private SharedPreferences sharedPref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_requests);

        device_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        sharedPref = getSharedPreferences(
                getString(R.string.preference_account_details), Context.MODE_PRIVATE);

        // toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // views
        viewStub = (ViewStub) findViewById(R.id.friend_requests_list_empty);
        listView = (ListView) findViewById(R.id.friend_requests_list);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.friend_requests_swipe_refresh_layout);

        // adapter
        requestsAdapter = new FriendRequestsAdapter(requests, this, this);
        listView.setAdapter(requestsAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateRequestsList();
            }
        });

        updateRequestsList();
    }

    public void updateRequestsList(){

        Log.d("CHEGOU", "FriendRequestsActivity ->\n\t updateRequestsList()");

        swipeRefreshLayout.setRefreshing(true);

        GetRequestsListTask grlt = new GetRequestsListTask(
                this,
                sharedPref.getInt(getString(R.string.saved_id), 0),
                device_id,
                sharedPref.getString(getString(R.string.saved_token), "")
                );

        grlt.execute();

    }

    @Override
    public void processGetRequestsListTask(String s) {
        Log.d("VALUE", "FriendRequestsActivity ->\n\t processGetRequestsListTask() ->\n\t\t String s = " + s);
        try {

            // Parse JSON
            JSONObject jsonObject = new JSONObject(s);

            int error = jsonObject.getInt("error");

            Log.d("VALUE", "FriendRequestsActivity ->\n\t processGetRequestsListTask() ->\n\t\t int error (from parser) = " + error);

            // verifica se houve erro
            if(error == 0) {

                requests.clear();

                JSONArray requestsArray = (JSONArray) jsonObject.getJSONArray("requests_list");
                if (requestsArray != null) {
                    for (int i=0; i<requestsArray.length(); i++){

                        requests.add(requestsArray.getInt(i));
                    }
                }

            }else{

                switch(error) {
                    case SharedMethods.CONNECTION_ERROR:
                        AlertDialog errorConnDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_10_message);
                        break;

                    case SharedMethods.INVALID_TOKEN_ERROR:
                        AlertDialog errorTokenDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_14_message);
                        break;

                    default:
                        AlertDialog errorDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_25_message);
                }
            }

        }catch(JSONException e){
            e.printStackTrace();
        }

        // atualiza views

        requestsAdapter.notifyDataSetChanged();

        if(requestsAdapter.getCount() == 0) {
            Log.d("CHEGOU", "FriendRequestsActivity ->\n\t requestsAdapter.getCount() == 0");

            if (viewStub.getParent() != null) {
                Log.d("CHEGOU", "FriendRequestsActivity ->\n\t viewStub.getParent() != null ->\n\t\t viewStub.inflate()");
                viewStub.inflate();
            } else {
                Log.d("CHEGOU", "FriendRequestsActivity ->\n\t viewStub.getParent() == null ->\n\t\t viewStub.setVisibility(View.VISIBLE) ");
                Log.d("CHEGOU", "FriendRequestsActivity ->\n\t viewStub.getParent() == null");
                viewStub.setVisibility(View.VISIBLE);
            }
        }
        else {
            Log.d("CHEGOU", "FriendRequestsActivity ->\n\t requestsAdapter.getCount != 0 ->\n\t\t viewStub.setVisibility(View.GONE); ");
            viewStub.setVisibility(View.GONE);
        }

        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onClick(View view) {
        Log.d("CHEGOU", "FriendRequestsActivity ->\n\t onClick()");
        Log.d("VALUE", "FriendRequestsActivity ->\n\t onClick() ->\n\t\t int view.getId() = " + view.getId());

        boolean answer = false;

        switch(view.getId()){
            case R.id.friend_requests_item_btn_allow:
                answer = true;

            case R.id.friend_requests_item_btn_deny:

                view.setEnabled(false);

                RespondFriendRequestTask rfrt2 = new RespondFriendRequestTask(
                        this,
                        sharedPref.getInt(getString(R.string.saved_id), 0),
                        device_id,
                        sharedPref.getString(getString(R.string.saved_token), ""),
                        (int) view.getTag(),
                        answer
                );
                rfrt2.execute();
                break;
        }
    }

    @Override
    public void processRespondFriendRequestTask(String s) {
        Log.d("VALUE", "FriendRequestsActivity ->\n\t processRespondFriendRequestTask() ->\n\t\t String s = " + s);
        try {

            // Parse JSON
            JSONObject jsonObject = new JSONObject(s);

            int error = jsonObject.getInt("error");

            Log.d("VALUE", "FriendRequestsActivity ->\n\t processRespondFriendRequestTask() ->\n\t\t int error (from parser) = " + error);

            // verifica se houve erro
            if(error == 0) {

                updateRequestsList();

            }else{

                switch(error) {
                    case SharedMethods.CONNECTION_ERROR:
                        AlertDialog errorConnDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_10_message);
                        break;

                    case SharedMethods.INVALID_TOKEN_ERROR:
                        AlertDialog errorTokenDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_14_message);
                        break;

                    default:
                        AlertDialog errorDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_26_message);
                }
            }

        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClickPositiveButton(int error) {

    }

    @Override
    public void onClickNegativeButton(int error) {

    }
}
