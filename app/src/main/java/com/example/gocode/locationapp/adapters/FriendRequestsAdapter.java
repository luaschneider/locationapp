package com.example.gocode.locationapp.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.SharedMethods;
import com.example.gocode.locationapp.activities.FriendRequestsActivity;
import com.example.gocode.locationapp.models.Friend;
import com.example.gocode.locationapp.tasks.RespondFriendRequestTask;

import java.util.ArrayList;

public class FriendRequestsAdapter extends BaseAdapter {

    public static int ID_VIEW_TAG = 42;

    private ArrayList<Integer> requests_list;
    private Context context;
    private View.OnClickListener onClickListenerCallback;

    public FriendRequestsAdapter(ArrayList<Integer> requests_list, Context context, View.OnClickListener onClickListenerCallback) {
        this.requests_list = requests_list;
        this.context = context;
        this.onClickListenerCallback = onClickListenerCallback;
    }

    @Override
    public int getCount() {
        return requests_list.size();
    }

    @Override
    public Object getItem(int i) {
        return requests_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;

        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.list_friend_requests_item_layout, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        int from_id = requests_list.get(position);

        holder.id.setText(SharedMethods.getFormattedId(from_id));

        holder.btn_allow.setTag(from_id);
        holder.btn_deny.setTag(from_id);

        holder.btn_allow.setOnClickListener(onClickListenerCallback);
        holder.btn_deny.setOnClickListener(onClickListenerCallback);


        return view;
    }

    public class ViewHolder {

        final TextView id;
        final Button btn_allow, btn_deny;

        public ViewHolder(View view) {
            id        = (TextView) view.findViewById(R.id.friend_requests_item_txt_id);
            btn_allow = (Button) view.findViewById(R.id.friend_requests_item_btn_allow);
            btn_deny = (Button) view.findViewById(R.id.friend_requests_item_btn_deny);
        }
    }
}