package com.example.gocode.locationapp.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.gocode.locationapp.DataManager;
import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.SharedMethods;
import com.example.gocode.locationapp.adapters.FriendsAdapter;
import com.example.gocode.locationapp.models.Friend;
import com.example.gocode.locationapp.tasks.GetFriendsListTask;
import com.example.gocode.locationapp.tasks.UnfriendTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FriendsFragment extends Fragment implements  UnfriendTask.UnfriendTaskCallback, GetFriendsListTask.GetFriendsListTaskCallback, AdapterView.OnItemClickListener, SharedMethods.ErrorDialogCallback {

    private ViewStub viewStubFriends;
    private FriendsAdapter friendsAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    private String device_id;
    private SharedPreferences sharedPref;

    private DataManager dataManager;
    private ArrayList<Friend> friends = new ArrayList<>();

    public FriendsFragment() {
        // Required empty public constructor
    }

    public static FriendsFragment newInstance() {
        FriendsFragment fragment = new FriendsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        device_id = Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        sharedPref = getContext().getSharedPreferences(
                getString(R.string.preference_account_details), Context.MODE_PRIVATE);

        dataManager = new DataManager(getContext(), DataManager.FRIENDS_NAMES_DB_PATH);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends, container, false);

        viewStubFriends    = (ViewStub) view.findViewById(R.id.friends_list_empty);
        ListView listView  = (ListView) view.findViewById(R.id.friends_list);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.friends_swipe_refresh_layout);

        friendsAdapter = new FriendsAdapter(friends, getContext());
//        updateFriendsList(getContext());



        listView.setAdapter(friendsAdapter);
        listView.setOnItemClickListener(this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateFriendsList();
            }
        });
        updateFriendsList();

        return view;
    }

    public void updateFriendsList(){

        Log.d("CHEGOU", "FriendsFragment ->\n\t updateFriendsList()");

        swipeRefreshLayout.setRefreshing(true);

        GetFriendsListTask gflt = new GetFriendsListTask(
                this,
                sharedPref.getInt(getString(R.string.saved_id), 0),
                device_id,
                sharedPref.getString(getString(R.string.saved_token), "")
        );

        gflt.execute();

    }

    @Override
    public void processGetFriendsListTask(String s) {

        Log.d("VALUE", "FriendRequestsActivity ->\n\t processGetRequestsListTask() ->\n\t\t String s = " + s);
        try {

            // Parse JSON
            JSONObject jsonObject = new JSONObject(s);

            int error = jsonObject.getInt("error");

            Log.d("VALUE", "FriendRequestsActivity ->\n\t processGetRequestsListTask() ->\n\t\t int error (from parser) = " + error);

            // verifica se houve erro
            if(error == 0) {

                friends.clear();

                JSONArray friendsArray = (JSONArray) jsonObject.getJSONArray("friends_list");
                if (friendsArray != null) {
                    for (int i=0; i<friendsArray.length(); i++){

                        String formatted_id = SharedMethods.getFormattedId(friendsArray.getInt(i));

                        Friend f = new Friend(formatted_id, formatted_id);

                        if(dataManager.doesObjectExist(formatted_id)){
                            f.setCustomName(dataManager.getObjectName(formatted_id));
                        }else{
                            dataManager.addObject(f);
                        }

                        friends.add(f);
                    }
                }

            }else{

                switch(error) {
                    case SharedMethods.CONNECTION_ERROR:
                        AlertDialog errorConnDialog = SharedMethods.showErrorDialog(getContext(), this, error, R.string.dialog_error_10_message);
                        break;

                    case SharedMethods.INVALID_TOKEN_ERROR:
                        AlertDialog errorTokenDialog = SharedMethods.showErrorDialog(getContext(), this, error, R.string.dialog_error_14_message);
                        break;

                    default:
                        AlertDialog errorDialog = SharedMethods.showErrorDialog(getContext(), this, error, R.string.dialog_error_27_message);
                }
            }

        }catch(JSONException e){
            e.printStackTrace();
        }

        // atualiza views

        Collections.sort(friends, new Comparator<Friend>() {
            @Override
            public int compare(Friend friend1, Friend friend2) {
                return friend1.getCustomName().compareTo(friend2.getCustomName());
            }
        });

        friendsAdapter.notifyDataSetChanged();

        if(friendsAdapter.getCount() == 0) {
            if (viewStubFriends.getParent() != null) {
                viewStubFriends.inflate();
            } else {
                viewStubFriends.setVisibility(View.VISIBLE);
            }
        }
        else {
            viewStubFriends.setVisibility(View.GONE);
        }

        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        // pega o nome do amigo
        final String item_name = ((TextView) view.findViewById(R.id.friends_item_txt_name)).getText().toString();

        // pega o id do amigo
        final String item_id = ((TextView) view.findViewById(R.id.friends_item_txt_id)).getText().toString();


        // constrói o dialog
        AlertDialog.Builder itemDialogBuilder = new AlertDialog.Builder(getActivity());
        itemDialogBuilder.setTitle(item_name)
                .setItems(R.array.main_friends_list_item_options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0: // item "edit name"

                                AlertDialog.Builder editItemBuilder = new AlertDialog.Builder(getActivity());

                                View editItemDialogView = View.inflate(getContext(), R.layout.fragment_edit_name_dialog, null);
                                final EditText edt_name = (EditText) editItemDialogView.findViewById(R.id.edit_name_dialog_edt_name);
                                edt_name.setText(item_name);
                                edt_name.requestFocus();

                                editItemBuilder.setTitle("Editar nome")
                                        .setView(editItemDialogView)
                                        .setPositiveButton("Salvar", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {

                                                String new_name = edt_name.getText().toString();

                                                if(!new_name.isEmpty()){

                                                    dataManager.objectChangeName(new_name, item_id);

                                                    updateFriendsList();
                                                }

                                            }
                                        })
                                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.cancel();
                                            }
                                        })
                                        .show();

                                break;

                            case 1: // item "see on map"
                                break;

                            case 2: //item "unfriend"

                                AlertDialog.Builder unfriendDialodBuilder = new AlertDialog.Builder(getActivity());

                                unfriendDialodBuilder.setTitle(R.string.main_friends_dialog_unfriend_title)
                                        .setMessage(String.format(getContext().getString(R.string.main_friends_dialog_unfriend_message), item_name))
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                UnfriendTask ut = new UnfriendTask(FriendsFragment.this,
                                                        sharedPref.getInt(getString(R.string.saved_id), 0),
                                                        device_id,
                                                        sharedPref.getString(getString(R.string.saved_token), ""),
                                                        SharedMethods.getIntFromFormattedId(item_id));
                                                ut.execute();
                                            }
                                        })
                                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.cancel();
                                            }
                                        })
                                        .show();

                                break;
                        }
                    }
                }).show();
    }

    @Override
    public void processUnfriendTask(String s) {
        Log.d("VALUE", "FriendRequestsActivity ->\n\t processUnfriendTask() ->\n\t\t String s = " + s);
        try {

            // Parse JSON
            JSONObject jsonObject = new JSONObject(s);

            int error = jsonObject.getInt("error");

            Log.d("VALUE", "FriendRequestsActivity ->\n\t processUnfriendTask() ->\n\t\t int error (from parser) = " + error);

            // verifica se houve erro
            if(error == 0) {

                updateFriendsList();

            }else{

                switch(error) {
                    case SharedMethods.CONNECTION_ERROR:
                        AlertDialog errorConnDialog = SharedMethods.showErrorDialog(getContext(), this, error, R.string.dialog_error_10_message);
                        break;

                    case SharedMethods.INVALID_TOKEN_ERROR:
                        AlertDialog errorTokenDialog = SharedMethods.showErrorDialog(getContext(), this, error, R.string.dialog_error_14_message);
                        break;

                    default:
                        AlertDialog errorDialog = SharedMethods.showErrorDialog(getContext(), this, error, R.string.dialog_error_28_message);
                }
            }

        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClickPositiveButton(int error) {

    }

    @Override
    public void onClickNegativeButton(int error) {

    }
}

