package com.example.gocode.locationapp.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.SharedMethods;

public class HintsIdFragment extends Fragment {

    private TextView txt_id;
    private HintsIdCallback mListener;

    public HintsIdFragment() {
        // Required empty public constructor
    }

    public static HintsIdFragment newInstance() {
        HintsIdFragment fragment = new HintsIdFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            mListener = (HintsIdCallback) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement OnMapClickListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hints_id, container, false);

        txt_id = (TextView) view.findViewById(R.id.hints_id_id);

        mListener.onIdFragmentCreated();

        return view;
    }

    // SET ID METHOD
    public void setIdTxtValue(int id){
        txt_id.setText(SharedMethods.getFormattedId(id));
    }

    // CALLBACK
    public interface HintsIdCallback {
        void onIdFragmentCreated();
    }

}
