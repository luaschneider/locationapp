package com.example.gocode.locationapp.services;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by Go Code on 28/02/2017.
 */

public class TrackerService extends IntentService{

    // Must create a default constructor
    public TrackerService() {
        // Used to name the worker thread, important only for debugging.
        super("tracker-service");
    }

    @Override
    protected void onHandleIntent(Intent workIntent) {
        // Gets data from the incoming Intent
        String dataString = workIntent.getDataString();

        // Do work here, based on the contents of dataString
    }

}
