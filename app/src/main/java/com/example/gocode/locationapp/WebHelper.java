package com.example.gocode.locationapp;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Go Code on 05/11/2016.
 */

public class WebHelper {

    public static final String HOST_NAME = "http://locationapp.pe.hu/";
    public static final String IS_DEVICE_REGISTERED_FILE = "req/is_device_registered.php";
    public static final String PRE_REGISTER_USER_FILE = "req/pre_register_user.php";
    public static final String CHECK_USER_DATA_FILE = "req/check_user_data.php";
    public static final String REGISTER_USER_FILE = "req/register_user.php";
    public static final String SEND_FRIEND_REQUEST_FILE = "req/send_friend_request.php";
    public static final String GET_REQUESTS_LIST_FILE = "req/get_friend_requests.php";
    public static final String RESPOND_FRIEND_REQUEST_FILE = "req/respond_friend_request.php";
    public static final String GET_FRIENDS_LIST_FILE = "req/get_friends_list.php";
    public static final String UNFRIEND_FILE = "req/unfriend.php";
    public static final String REPORT_ERROR_FILE = "req/report_error.php";

}
