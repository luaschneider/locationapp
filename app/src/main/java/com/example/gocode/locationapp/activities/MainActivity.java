package com.example.gocode.locationapp.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.SharedMethods;
import com.example.gocode.locationapp.adapters.MainPagerAdapter;
import com.example.gocode.locationapp.tasks.GetRequestsListTask;
import com.example.gocode.locationapp.tasks.SendFriendRequestTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements SendFriendRequestTask.SendFriendRequestTaskCallback, GetRequestsListTask.GetRequestsListTaskCallback, SharedMethods.ErrorDialogCallback {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MainPagerAdapter viewPagerAdapter;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private Menu menu;

    private final int FRIENDS_FRAG_POSITION = 2;
    private final int MAP_FRAG_POSITION = 1;
    private final int DETAILS_FRAG_POSITION = 0;

    private int[] tabIcons = {
            R.drawable.ic_tab_details,
            R.drawable.ic_tab_map,
            R.drawable.ic_tab_friends
    };

    private String device_id;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        device_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        sharedPref = getSharedPreferences(
                getString(R.string.preference_account_details), Context.MODE_PRIVATE);

        //TOOLBAR
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        //FLOATING ACTION BUTTON
        fab = (FloatingActionButton) findViewById(R.id.main_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.main_send_request_dialog_title);
                builder.setMessage(R.string.main_send_request_dialog_message);

                View addFriendDialogView = View.inflate(MainActivity.this, R.layout.fragment_send_request_dialog, null);

                builder.setView(addFriendDialogView);

                final EditText edt_id_1 = (EditText) addFriendDialogView.findViewById(R.id.add_friend_dialog_edt_id_1);
                final EditText edt_id_2 = (EditText) addFriendDialogView.findViewById(R.id.add_friend_dialog_edt_id_2);
                final EditText edt_id_3 = (EditText) addFriendDialogView.findViewById(R.id.add_friend_dialog_edt_id_3);

                edt_id_1.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if(charSequence.length() == 3){
                            edt_id_2.requestFocus();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                edt_id_2.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if(charSequence.length() == 3){
                            edt_id_3.requestFocus();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });


                // Set up the buttons
                builder.setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String to_id_str = edt_id_1.getText().toString() + edt_id_2.getText().toString() + edt_id_3.getText().toString();
                        Log.d("VALUE", "MainActivity ->\n\t String to_id_str = " + to_id_str);

                        if(to_id_str.length() == 9) {
                            int to_id = Integer.parseInt(to_id_str);

                            SendFriendRequestTask sfrt =
                                    new SendFriendRequestTask(
                                            MainActivity.this,
                                            sharedPref.getInt(getString(R.string.saved_id), 0),
                                            device_id,
                                            sharedPref.getString(getString(R.string.saved_token), ""),
                                            to_id
                                    );
                            sfrt.execute();
                        }else{
                            Toast.makeText(MainActivity.this, R.string.main_send_request_invalid_error, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        //VIEW PAGER
        viewPager = (ViewPager) findViewById(R.id.main_viewpager);
        viewPagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(viewPagerAdapter);

        //TAB LAYOUT
        tabLayout = (TabLayout) findViewById(R.id.main_sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLayout.getTabAt(i).setIcon(tabIcons[i]);
        }
        tabLayout.getTabAt(DETAILS_FRAG_POSITION).getIcon().setAlpha(198);
        tabLayout.getTabAt(MAP_FRAG_POSITION).getIcon().setAlpha(88);
        tabLayout.getTabAt(FRIENDS_FRAG_POSITION).getIcon().setAlpha(88);

        if (tabLayout.getSelectedTabPosition() != 1) {
            fab.hide();
        }


        //VIEW PAGER ON PAGE CHANGE LISTENER
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int name;
                switch (position) {
                    case DETAILS_FRAG_POSITION:
                        name = R.string.main_tablayout_title_details;
                        tabLayout.getTabAt(DETAILS_FRAG_POSITION).getIcon().setAlpha(198);
                        tabLayout.getTabAt(MAP_FRAG_POSITION).getIcon().setAlpha(88);
                        tabLayout.getTabAt(FRIENDS_FRAG_POSITION).getIcon().setAlpha(88);
                        fab.hide();
                        break;
                    case MAP_FRAG_POSITION:
                        name = R.string.main_tablayout_title_map;
                        tabLayout.getTabAt(DETAILS_FRAG_POSITION).getIcon().setAlpha(88);
                        tabLayout.getTabAt(MAP_FRAG_POSITION).getIcon().setAlpha(198);
                        tabLayout.getTabAt(FRIENDS_FRAG_POSITION).getIcon().setAlpha(88);
                        fab.hide();
                        break;
                    default:
                        name = R.string.main_tablayout_title_friends;
                        tabLayout.getTabAt(DETAILS_FRAG_POSITION).getIcon().setAlpha(88);
                        tabLayout.getTabAt(MAP_FRAG_POSITION).getIcon().setAlpha(88);
                        tabLayout.getTabAt(FRIENDS_FRAG_POSITION).getIcon().setAlpha(198);
                        fab.show();
                }
                getSupportActionBar().setTitle(name);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.drawable.ic_stat_icon)
//                        .setContentTitle(getString(R.string.app_name))
//                        .setContentText(
//                                String.format(
//                                        getString(R.string.not_approve_message),
//                                        getString(R.string.test_id_2)
//                                )
//                        )
//                        .addAction(R.drawable.ic_close_white, getString(R.string.not_approve_btn_negative), null) // #0
//                        .addAction(R.drawable.ic_check_white, getString(R.string.not_approve_btn_positive), null)  // #1
//                        .setLargeIcon(
//                                BitmapFactory.decodeResource(getResources(),
//                                        R.drawable.ic_stat_icon)
//                        );
//
//        // Sets an ID for the notification
//        int mNotificationId = 001;
//        // Gets an instance of the NotificationManager service
//        NotificationManager mNotifyMgr =
//                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        // Builds the notification and issues it.
//        mNotifyMgr.notify(mNotificationId, mBuilder.build());

    }

    @Override
    protected void onResume() {
        super.onResume();

        //CHECK FOR FRIEND REQUESTS
        GetRequestsListTask grlt = new GetRequestsListTask(
                this,
                sharedPref.getInt(getString(R.string.saved_id), 0),
                device_id,
                sharedPref.getString(getString(R.string.saved_token), "")
        );

//        grlt.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(getApplication()).inflate(R.menu.menu_action_main, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.action_requests:
                Intent intent = new Intent(this, FriendRequestsActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processSendFriendRequestTask(String s) {
        Log.d("VALUE", "MainActivity ->\n\t processSendFriendRequestTask() ->\n\t\t String s = " + s);
        try {

            // Parse JSON
            JSONObject jsonObject = new JSONObject(s);

            int error = jsonObject.getInt("error");

            Log.d("VALUE", "MainActivity ->\n\t processSendFriendRequestTask() ->\n\t\t int error (from parser) = " + error);

            // verifica se houve erro
            if(error == 0) {

                Toast.makeText(this, R.string.main_send_request_sucess, Toast.LENGTH_LONG).show();

            }else{

                switch(error) {
                    case SharedMethods.CONNECTION_ERROR:
                        AlertDialog errorConnDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_10_message);
                        break;
                    case SharedMethods.INVALID_TOKEN_ERROR:
                        AlertDialog errorTokenDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_14_message);
                        break;

                    case SharedMethods.INVALID_OTHER_ACCOUNT_ERROR:
                        Toast.makeText(this, R.string.error_15_message, Toast.LENGTH_LONG).show();
                        break;

                    case SharedMethods.ALREADY_REQUESTED_FRIENDSHIP_ERROR:
                        Toast.makeText(this, R.string.error_16_message, Toast.LENGTH_LONG).show();
                        break;

                    case SharedMethods.OTHER_ID_EQUALS_USERS_ERROR:
                        Toast.makeText(this, R.string.error_17_message, Toast.LENGTH_LONG).show();
                        break;

                    case SharedMethods.ALREADY_FRIENDS_ERROR:
                        Toast.makeText(this, R.string.error_110_message, Toast.LENGTH_LONG).show();
                        break;

                    default:
                        AlertDialog errorDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_24_message);
                }
            }

        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void processGetRequestsListTask(String s) {
        Log.d("VALUE", "MainActivity ->\n\t processGetRequestsListTask() ->\n\t\t String s = " + s);
        try {

            // Parse JSON
            JSONObject jsonObject = new JSONObject(s);

            int error = jsonObject.getInt("error");

            Log.d("VALUE", "MainActivity ->\n\t processGetRequestsListTask() ->\n\t\t int error (from parser) = " + error);

            // verifica se houve erro
            if(error == 0) {

                JSONArray requestsArray = (JSONArray) jsonObject.getJSONArray("requests_list");
                if (requestsArray != null && requestsArray.length() > 0) {
                    menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.ic_notifications_ringing));
                }else{
                    menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.ic_notifications_empty));
                }

            }else{

                switch(error) {
                    case SharedMethods.CONNECTION_ERROR:
                        AlertDialog errorConnDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_10_message);
                        break;

                    case SharedMethods.INVALID_TOKEN_ERROR:
                        AlertDialog errorTokenDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_14_message);
                        break;

                    default:
                        AlertDialog errorDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_25_message);
                }
            }

        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClickPositiveButton(int error) {

    }

    @Override
    public void onClickNegativeButton(int error) {

    }
}
