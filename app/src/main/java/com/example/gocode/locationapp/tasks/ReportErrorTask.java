package com.example.gocode.locationapp.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.gocode.locationapp.WebHelper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Go Code on 10/11/2016.
 */

public class ReportErrorTask extends AsyncTask<Void, Void, String> {

    private ReportErrorTaskCallback callback;
    private String device_id;
    private String token;
    private int account_id;
    private int error_code;
    private String class_name;

    public ReportErrorTask(ReportErrorTaskCallback callback, int account_id, String device_id, String token, int error_code, String class_name) {
        this.callback   = callback;
        this.account_id = account_id;
        this.device_id  = device_id;
        this.token      = token;
        this.error_code = error_code;
        this.class_name = class_name;
    }

    @Override
    protected String doInBackground(Void... voids) {

        HttpURLConnection connection = null;
        try {

            URL url = new URL(WebHelper.HOST_NAME + WebHelper.REPORT_ERROR_FILE);
            String urlParameters =
                    "device_id=" + URLEncoder.encode(device_id, "UTF-8") +
                            "&token=" + URLEncoder.encode(token, "UTF-8") +
                            "&error_code=" + URLEncoder.encode(String.valueOf(error_code), "UTF-8") +
                            "&class_name=" + URLEncoder.encode(class_name, "UTF-8") +
                            "&account_id=" + URLEncoder.encode(String.valueOf(account_id), "UTF-8") ;
            Log.d("VALUE", "ReportErrorTask ->\n\t String device_id = "+ device_id);
            Log.d("VALUE", "ReportErrorTask ->\n\t String token = "+ token);
            Log.d("VALUE", "ReportErrorTask ->\n\t int account_id = "+ account_id);
            Log.d("VALUE", "ReportErrorTask ->\n\t int error_code = "+ error_code);
            Log.d("VALUE", "ReportErrorTask ->\n\t String class_name = "+ class_name);

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(5000);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            //Send request
            DataOutputStream wr = new DataOutputStream (
                    connection.getOutputStream ());
            wr.writeBytes (urlParameters);
            wr.flush ();
            wr.close ();

            connection.connect();

            Log.d("VALUE", "ReportErrorTask ->\n\t int connection.getResponseCode() = "+ connection.getResponseCode());

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                Log.d("CHEGOU", "ReportErrorTask ->\n\t connection.getRespondeCode() = " + HttpURLConnection.HTTP_OK);
                InputStream inputStream = connection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();
                connection.disconnect();

                String strResult = sb.toString().substring(1);
                Log.d("VALUE", "ReportErrorTask ->\n\t String strResult = " + strResult);
                return strResult;
            }

        }catch(Exception e){
            e.printStackTrace();
        }finally{
            if(connection != null){
                connection.disconnect();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(s == null){
            s = "{\"error\" : 10}";
        }else if(s.isEmpty()){
            s = "{\"error\" : 10}";
        }

        callback.processReportErrorTask(s);

    }

    public interface ReportErrorTaskCallback{
        void processReportErrorTask(String s);
    }
}
