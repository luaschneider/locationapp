package com.example.gocode.locationapp.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.gocode.locationapp.WebHelper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Go Code on 10/11/2016.
 */

public class RespondFriendRequestTask extends AsyncTask<Void, Void, String> {

    private RespondFriendRequestTaskCallback callback;
    private String device_id;
    private String token;
    private int account_id;
    private int from_id;
    private boolean isAnswerAllow;

    public RespondFriendRequestTask(RespondFriendRequestTaskCallback callback, int account_id, String device_id, String token, int from_id, boolean isAnswerAllow) {
        this.callback = callback;
        this.account_id = account_id;
        this.device_id = device_id;
        this.token = token;
        this.from_id = from_id;
        this.isAnswerAllow = isAnswerAllow;
    }

    @Override
    protected String doInBackground(Void... voids) {

        HttpURLConnection connection = null;
        try {

            URL url = new URL(WebHelper.HOST_NAME + WebHelper.RESPOND_FRIEND_REQUEST_FILE);
            String urlParameters =
                    "device_id=" + URLEncoder.encode(device_id, "UTF-8") +
                            "&token=" + URLEncoder.encode(token, "UTF-8") +
                            "&from_id=" + URLEncoder.encode(String.valueOf(from_id), "UTF-8") +
                            "&answer=" + URLEncoder.encode(String.valueOf(isAnswerAllow), "UTF-8") +
                            "&account_id=" + URLEncoder.encode(String.valueOf(account_id), "UTF-8") ;
            Log.d("VALUE", "RespondFriendRequestTask ->\n\t String device_id = "+ device_id);
            Log.d("VALUE", "RespondFriendRequestTask ->\n\t String token = "+ token);
            Log.d("VALUE", "RespondFriendRequestTask ->\n\t int account_id = "+ account_id);
            Log.d("VALUE", "RespondFriendRequestTask ->\n\t int from_id = "+ from_id);
            Log.d("VALUE", "RespondFriendRequestTask ->\n\t boolean answer = "+ String.valueOf(isAnswerAllow));

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(5000);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            //Send request
            DataOutputStream wr = new DataOutputStream (
                    connection.getOutputStream ());
            wr.writeBytes (urlParameters);
            wr.flush ();
            wr.close ();

            connection.connect();

            Log.d("VALUE", "RespondFriendRequestTask ->\n\t int connection.getResponseCode() = "+ connection.getResponseCode());

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                Log.d("CHEGOU", "RespondFriendRequestTask ->\n\t connection.getRespondeCode() = " + HttpURLConnection.HTTP_OK);
                InputStream inputStream = connection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();
                connection.disconnect();

                String strResult = sb.toString().substring(1);
                Log.d("VALUE", "RespondFriendRequestTask ->\n\t String strResult = " + strResult);
                return strResult;
            }

        }catch(Exception e){
            e.printStackTrace();
        }finally{
            if(connection != null){
                connection.disconnect();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(s == null){
            s = "{\"error\" : 10}";
        }else if(s.isEmpty()){
            s = "{\"error\" : 10}";
        }

        callback.processRespondFriendRequestTask(s);

    }

    public interface RespondFriendRequestTaskCallback{
        void processRespondFriendRequestTask(String s);
    }
}
