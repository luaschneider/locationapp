package com.example.gocode.locationapp.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.SharedMethods;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    MapView mMapView;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;

    private final int REQUEST_LOCATION = 42;

    private Marker locationMarker;
    private Location myLocation;
    private LocationRequest mLocationRequest;
    private PendingResult result;

    public MapFragment() {
        // Required empty public constructor
    }

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedPreferences sharedPref = getContext().getSharedPreferences(
                getString(R.string.preference_account_details), Context.MODE_PRIVATE);

        View view = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


        mGoogleApiClient.connect();

//        AddMarkersTask addMarkersTask = new AddMarkersTask(this, getContext());
//        addMarkersTask.execute(localArrayList);

//        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
//
//                if(!marker.getTitle().equals(getString(R.string.map_marker_current))){
//                    Local local = localArrayList.get(Integer.parseInt(marker.getTitle()));
//
//                    mListener.onMarkerClickListener(local);
//
//                    return true;
//                }
//
//                return false;
//            }
//        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        updateMap();
    }

    private void updateMap() {

        LatLng poa = new LatLng(-30.036553, -51.172803);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(poa, 12));

//        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(getActivity(),
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                    REQUEST_LOCATION);
//
//        } else {
//            myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//
//            if(myLocation != null && locationMarker == null){
//                LatLng myLocationLL = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
//                MarkerOptions locationMarkerOptions = new MarkerOptions()
//                        .position(myLocationLL)
//                        .title("Você");
////                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_current));
//
//                locationMarker = mMap.addMarker(locationMarkerOptions);
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocationLL, 17));
//
//            }else {
//                LatLng poa = new LatLng(-30.036553, -51.172803);
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(poa, 12));
//            }
//
//            mLocationRequest = new LocationRequest()
//                    .setInterval(5000)
//                    .setFastestInterval(3000)
//                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
//
//            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
//                    .addLocationRequest(mLocationRequest);
//            builder.setAlwaysShow(true);
//
//            result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
//            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
//                @Override
//                public void onResult(@NonNull LocationSettingsResult result) {
//                    final Status status = result.getStatus();
//                    switch (status.getStatusCode()) {
//                        case LocationSettingsStatusCodes.SUCCESS:
//                            break;
//                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            Snackbar.make(getActivity().findViewById(R.id.main_coordinator), R.string.map_request_gps_message, Snackbar.LENGTH_INDEFINITE)
//                                    .setAction(R.string.map_request_gps_ativar, new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            try {
//                                                status.startResolutionForResult(getActivity(), REQUEST_LOCATION);
//                                            } catch (IntentSender.SendIntentException e) {
//
//                                            }
//                                        }
//                                    }).setActionTextColor(Color.YELLOW)
//                                    .show();
//                            break;
//                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                            break;
//                    }
//                }
//            });
//
//            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new LocationListener() {
//                @Override
//                public void onLocationChanged(Location location) {
//                    if (locationMarker != null) {
//                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//                        locationMarker.setPosition(latLng);
//                    }
//                }
//            });
//        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStart() {
        if(mGoogleApiClient != null){
            mGoogleApiClient.connect();
        }

        super.onStart();
    }

    @Override
    public void onStop() {
        if(mGoogleApiClient != null){
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
