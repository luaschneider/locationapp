package com.example.gocode.locationapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.gocode.locationapp.models.Friend;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Go Code on 25/01/2017.
 */

public class DataManager {

    public static final String FRIENDS_NAMES_DB_PATH = "data/friends_names_db.dat";

    private Context context;
    private File file_path;
    private String file_path_string;

    public DataManager(Context context, String file_path_string){
        this.context = context;
        this.file_path_string = file_path_string;
        this.file_path = new File(context.getFilesDir(), file_path_string);
    }

    public DataManager(Context context){
        this.context = context;
    }

    public ArrayList<Friend> readNamesFile(){
        ArrayList<Friend> friendsArrayList = new ArrayList<>();

        try {
            FileInputStream fis = new FileInputStream(file_path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            friendsArrayList = (ArrayList<Friend>) ois.readObject();

            ois.close();
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ERRO", "DataManager ->\n\t readNamesFile()");
        }

        Log.d("VALUE", "DataManager ->\n\treadNamesFile()\n\t\tpetArrayList.toString() = " + friendsArrayList.toString());

        return friendsArrayList;
    }

    public void addAll(ArrayList<Friend> friends){
        try {
            FileOutputStream fos = new FileOutputStream(file_path);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friends);

            oos.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ERRO", "DataManager ->\n\t addAll()");
        }
    }

    public void addObject(Friend newFriend){
        ArrayList<Friend> friendsArrayList = this.readNamesFile();
        friendsArrayList.add(newFriend);
        this.addAll(friendsArrayList);
    }

    public boolean doesObjectExist(String formatted_id){
        ArrayList<Friend> friendsArrayList = this.readNamesFile();

        for(Friend f: friendsArrayList){
            if(f.getId().equals(formatted_id)){
                return true;
            }
        }

        return false;
    }

    public String getObjectName(String formatted_id){
        ArrayList<Friend> friendsArrayList = this.readNamesFile();

        for(Friend f: friendsArrayList){
            if(f.getId().equals(formatted_id)){
                return f.getCustomName();
            }
        }

        return "";
    }

    public void objectChangeName(String newName, String friend_id){
        ArrayList<Friend> friendsArrayList = this.readNamesFile();

        for(Friend f : friendsArrayList){
            if(f.getId().equals(friend_id)){
                f.setCustomName(newName);
                break;
            }
        }

        this.addAll(friendsArrayList);
    }

}
