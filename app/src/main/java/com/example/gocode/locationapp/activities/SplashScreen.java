package com.example.gocode.locationapp.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.gocode.locationapp.DataManager;
import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.SharedMethods;
import com.example.gocode.locationapp.tasks.CheckDeviceRegisteredTask;
import com.example.gocode.locationapp.tasks.CheckUserDataTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SplashScreen extends AppCompatActivity implements CheckDeviceRegisteredTask.CheckDeviceRegisteredCallback, CheckUserDataTask.CheckUserDataCallback, SharedMethods.ErrorDialogCallback {

    private final int REQUEST_WIFI = 0;
    private String device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState   ) {
        super.onCreate(savedInstanceState);

        device_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("VALUE", "SplashScreen ->\n\t device_id = " + device_id);

        File names_db_file = new File(getFilesDir(), DataManager.FRIENDS_NAMES_DB_PATH);

        try {
            names_db_file.getParentFile().mkdirs();
            names_db_file.createNewFile();
            Log.d("CHEGOU","SplashScreen ->\n\tnames_db_file.createNewFile()");
        }catch(IOException e){
            Log.e("createNewFile()", e.getMessage());
        }

        checkConnectivityAndRun();


    }

    private void checkConnectivityAndRun(){

        if(SharedMethods.isConnected(this)){
            CheckDeviceRegisteredTask cdrt = new CheckDeviceRegisteredTask(this);
            cdrt.execute(device_id);
        }else{
            AlertDialog connectionErrorDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.alert_connection_title)
                .setMessage(R.string.alert_connection_message)

                .setPositiveButton(R.string.alert_connection_btn_settings, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                        startActivityForResult(intent, REQUEST_WIFI);
                    }
                })

                .setNegativeButton(R.string.alert_connection_btn_exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setIcon(R.drawable.icon)
                .setCancelable(false)
                .show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_WIFI){
            checkConnectivityAndRun();
        }

    }

    @Override
    public void processIsDeviceRegistered(String string) {


        try {
            // Parse JSON
            JSONObject jsonObject = new JSONObject(string);

            int error = jsonObject.getInt("error");
            Log.d("VALUE", "SplashScreen ->\n\t processIsDeviceRegistered() ->\n\t\t int error (from parser) = " + error);

            // verifica se houve erro
            if(error == 0) {

                boolean is_device_registered = jsonObject.getBoolean("is_device_registered");

                Log.d("VALUE", "SplashScreen ->\n\t processIsDeviceRegistered() ->\n\t\t boolean is_device_registered (from parser) = " + is_device_registered);

                if(is_device_registered) {
                    // caso o dispositivo esteja registrado
                    CheckUserDataTask cudt = new CheckUserDataTask(this, device_id);
                    cudt.execute();

                }else{
                    // caso não esteja
                    SharedMethods.startActivityAndFinish(this, this, WelcomeActivity.class);
                }

            }else{
                switch(error) {
                    case SharedMethods.CONNECTION_ERROR:
                        AlertDialog errorConnDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_10_message);
                        break;

                    default:
                        AlertDialog errorDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_20_message);
                }
            }

        }catch(JSONException e){
            AlertDialog errorJSONDialog = SharedMethods.showErrorDialog(this, this, SharedMethods.JSON_EXCEPTION_ERROR, R.string.dialog_error_20_message);
            e.printStackTrace();
        }
    }

    @Override
    public void processCheckUserData(String s) {
        try {

            // Parse JSON
            JSONObject jsonObject = new JSONObject(s);

            int error = jsonObject.getInt("error");

            Log.d("VALUE", "SplashScreen ->\n\t processCheckUserData() ->\n\t\t int error (from parser) = " + error);

            // verifica se houve erro
            if(error == 0) {

                int id = jsonObject.getInt("account_id");
                String token = jsonObject.getString("token");

                Log.d("VALUE", "SplashScreen ->\n\t processCheckUserData() ->\n\t\t int id (from parser) = " + id);
                Log.d("VALUE", "SplashScreen ->\n\t processCheckUserData() ->\n\t\t String token (from parser) = " + token);


                // Write preferences
                SharedPreferences sharedPref = this.getSharedPreferences(
                        getString(R.string.preference_account_details), Context.MODE_PRIVATE);
                SharedPreferences.Editor sharedPrefEditor = sharedPref.edit();

                sharedPrefEditor.putString(getString(R.string.saved_token), token);
                sharedPrefEditor.putInt(getString(R.string.saved_id), id);
                sharedPrefEditor.commit();

                SharedMethods.startActivityAndFinish(this, this, MainActivity.class);

            }else{

                switch(error) {
                    case SharedMethods.CONNECTION_ERROR:
                        AlertDialog errorConnDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_10_message);
                        break;

                    default:
                        AlertDialog errorDialog = SharedMethods.showErrorDialog(this, this, error, R.string.dialog_error_21_message);
                }
            }

        }catch(JSONException e){
            AlertDialog errorJSONDialog = SharedMethods.showErrorDialog(this, this, SharedMethods.JSON_EXCEPTION_ERROR, R.string.dialog_error_21_message);
            e.printStackTrace();
        }
    }

    @Override
    public void onClickPositiveButton(int error) {

    }

    @Override
    public void onClickNegativeButton(int error) {
        finish();
    }
}
