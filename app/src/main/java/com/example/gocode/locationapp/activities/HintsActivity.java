package com.example.gocode.locationapp.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.gocode.locationapp.R;
import com.example.gocode.locationapp.adapters.HintsPagerAdapter;
import com.example.gocode.locationapp.fragments.HintsIdFragment;
import com.viewpagerindicator.CirclePageIndicator;

public class HintsActivity extends AppCompatActivity implements View.OnClickListener, HintsIdFragment.HintsIdCallback {

    private ViewPager viewPager;
    private CirclePageIndicator circlePageIndicator;
    private Button btn_next, btn_ready, btn_previous;
    private HintsPagerAdapter hintsPagerAdapter;

    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hints);

        // Shared Preferences
        sharedPref = this.getSharedPreferences(
                getString(R.string.preference_account_details), Context.MODE_PRIVATE);

        // VIEWS
        circlePageIndicator = (CirclePageIndicator) findViewById(R.id.hints_indicator);
        viewPager = (ViewPager) findViewById(R.id.hints_viewpager);
        btn_next = (Button) findViewById(R.id.hints_next_button);
        btn_ready = (Button) findViewById(R.id.hints_ready_button);
        btn_previous = (Button) findViewById(R.id.hints_previous_button);

        // VIEWPAGER ADAPTER
        hintsPagerAdapter = new HintsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(hintsPagerAdapter);

        // PAGEINDICATOR
        circlePageIndicator.setViewPager(viewPager);

        // BUTTONS ONCLICK LISTENERS
        btn_next.setOnClickListener(this);
        btn_ready.setOnClickListener(this);
        btn_previous.setOnClickListener(this);

        // ONPAGECHANGE LISTENER
        circlePageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position) {
                    case 1:
                        btn_next.setVisibility(View.GONE);
                        btn_ready.setVisibility(View.VISIBLE);
                        btn_previous.setVisibility(View.VISIBLE);
                        break;

                    default:
                        btn_next.setVisibility(View.VISIBLE);
                        btn_ready.setVisibility(View.GONE);
                        btn_previous.setVisibility(View.GONE);
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.hints_next_button:
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                break;

            case R.id.hints_ready_button:
                this.setResult(RESULT_OK);
                finish();
                break;

            case R.id.hints_previous_button:
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                break;
        }
    }

    @Override
    public void onIdFragmentCreated() {
        int id = sharedPref.getInt(getString(R.string.saved_id), 0);
        Log.d("VALUE", "HitsActivity -> int id (from sharedPref) = " + id);
        hintsPagerAdapter.idFragmentSetter(id);
    }

}
