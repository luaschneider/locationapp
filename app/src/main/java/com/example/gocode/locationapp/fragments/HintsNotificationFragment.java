package com.example.gocode.locationapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.gocode.locationapp.R;

public class HintsNotificationFragment extends Fragment {

    public HintsNotificationFragment() {
        // Required empty public constructor
    }

    public static HintsNotificationFragment newInstance() {
        HintsNotificationFragment fragment = new HintsNotificationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_hints_notification, container, false);

    }

}
