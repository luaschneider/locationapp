package com.example.gocode.locationapp;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.example.gocode.locationapp.tasks.ReportErrorTask;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class SharedMethods {

    public static final String SPACEBAR = " ";

    // ERROR CODES
    public static final int CONNECTION_ERROR = 10;
    public static final int MISSING_PARAMS_ERROR = 11;
    public static final int NO_ACCOUNT_RELATED_ERROR = 12;
    public static final int NO_PRE_REGISTER_RECORD_ERROR = 13;
    public static final int INVALID_TOKEN_ERROR = 14;
    public static final int INVALID_OTHER_ACCOUNT_ERROR = 15;
    public static final int ALREADY_REQUESTED_FRIENDSHIP_ERROR = 16;
    public static final int OTHER_ID_EQUALS_USERS_ERROR = 17;
    public static final int DATA_BASE_CONNECTION_ERROR = 18;
    public static final int FRIEND_REQUEST_NOT_FOUND_ERROR = 19;
    public static final int ALREADY_FRIENDS_ERROR = 110;
    public static final int NO_FRIENDSHIP_ERROR = 111;
    public static final int JSON_EXCEPTION_ERROR = 112;

    // TASKS CODES
    public static final int CHECK_DEVICE_REGISTERED_CODE = 20;
    public static final int CHECK_USER_DATA_CODE = 21;
    public static final int PRE_REGISTER_USER_CODE = 22;
    public static final int REGISTER_USER_TASK = 23;
    public static final int SEND_FRIEND_REQUEST_TASK = 24;
    public static final int GET_REQUESTS_LIST_TASK = 25;
    public static final int RESPOND_FRIEND_REQUEST_TASK = 26;
    public static final int GET_FRIENDS_LIST_TASK = 27;
    public static final int UNFRIEND_TASK = 28;

    // METHODS
    public static final String md5_encrypt(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void startActivityAndFinish(Context context, Activity activity, Class cls){
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
        activity.finish();
    }

    public static boolean isConnected(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isConnected =
                activeNetwork != null &&
                activeNetwork.isAvailable() &&
                activeNetwork.isConnected();
        Log.d("VALUE", context.getPackageName() + " ->\n\tSharedMethos.isConnected() ->\n\t\t return = " + isConnected);
        return isConnected;
    }

    public static String getFormattedId(int id){
        String id_text = String.valueOf(id);

        id_text = id_text.substring(0, 3) +
                SPACEBAR +
                id_text.substring(3, 6) +
                SPACEBAR +
                id_text.substring(6);

        return id_text;
    }

    public static int getIntFromFormattedId(String id){
        id = id.replaceAll("\\s+", "");
        return Integer.parseInt(id);
    }

    public static AlertDialog showErrorDialog(Context context, final ErrorDialogCallback callback, final int error_code, int message_res_code){

        CharSequence message =
                String.format(
                    context.getString(message_res_code), error_code
                );


       return new AlertDialog.Builder(context)
                .setTitle(R.string.dialog_error_title)
                .setMessage(message)

                .setPositiveButton(R.string.dialog_error_btn_report, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    callback.onClickPositiveButton(error_code);
                    //TODO: criar task para enviar ao desenvolvedor
                    //ReportErrorTask ret = new ReportErrorTask(this, )
                    }
                })

                .setNegativeButton(R.string.dialog_error_btn_close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        callback.onClickNegativeButton(error_code);
                    }
                })
                .setIcon(R.drawable.icon)
//                .setCancelable(false)
                .show();

    }

    public interface ErrorDialogCallback {
        void onClickPositiveButton(int error);
        void onClickNegativeButton(int error);
    }
}
